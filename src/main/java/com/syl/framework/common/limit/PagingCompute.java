package com.syl.framework.common.limit;

import com.syl.framework.common.QueryCondition;

/**
 * 分页页数计算基础接口
 *
 * @author syl
 * @create 2018-03-27 20:47
 **/
public interface PagingCompute {

    /**
     * 实际数据库层面分页计算
     * @param pageCur 当前页
     * @param pageRow 每页显示几行
     */
     void compute(QueryCondition queryCondition, Integer pageCur, Integer pageRow);

}
