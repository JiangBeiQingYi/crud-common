package com.syl.framework.common.limit.impl;

import com.syl.framework.common.QueryCondition;
import com.syl.framework.common.limit.PagingCompute;

/**
 * @author syl
 * @create 2018-03-27 20:49
 **/
public class MySqlCompute implements PagingCompute {

    public void compute(QueryCondition queryCondition, Integer pageCur, Integer pageRow) {
        System.out.println(queryCondition.getOffset());
        queryCondition.setOffset((pageCur-1)*pageRow);
        queryCondition.setRow(pageRow);
    }
}
