package com.syl.framework.common;
 
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * 基础dao
 *
 * @author syl
 * @create 2018-03-17 22:10
 **/
public interface BaseDao<Bean,DTO> {

    /**
     *  选择性插入一条记录
     */
    int insertSelective(Bean bean);

    /**
     *  根据id主键查询记录
     *  @param id id
     */
    Bean selectById(@Param("id") String id);

    /**
     *  查询所有记录
     */
    List<Bean> selectAll();

    /**
     *  依据条件查询一条记录
     */
    Bean selectOne(@Param("condition") QueryCondition condition, @Param("bean") DTO bean);

    /**
     *  依据条件查询是否存在记录
     */
    boolean isExist(@Param("condition") QueryCondition condition, @Param("bean") DTO bean);

    /**
     *  依据条件查询记录列表
     */
    List<Bean> selectCondition(@Param("condition") QueryCondition condition, @Param("bean") DTO bean);

    /**
     *  依据条件查询记录数量
     */
    long selectConditionCount(@Param("condition") QueryCondition condition, @Param("bean") DTO bean);

}
