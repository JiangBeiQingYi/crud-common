package com.syl.framework.common.bean;

import java.util.List;

/**
 * 页面数据分页结果
 *
 * @author syl
 * @create 2018-03-17 21:53
 **/
public class DataPagingResult<Vo>{
    private List<Vo> data;
    private long pageCount;

    public DataPagingResult(List<Vo> data) {
        this.data = data;
    }
    public List<Vo> getData() {
        return data;
    }
    public long getPageCount() {
        return pageCount;
    }
    public DataPagingResult setPageCount(long pageCount) {
        this.pageCount = pageCount;
        return this;
    }
}
