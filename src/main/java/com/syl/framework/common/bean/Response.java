package com.syl.framework.common.bean;

/**
 * 统一应答类
 *
 * @author syl
 * @create 2018-03-17 21:06
 **/
public class Response<Result> {
    private int status;
    private Result result;
    private String message;
    private long timestamp;

    public int getStatus() {
        return status;
    }

    public Response setStatus(int status) {
        this.status = status;
        return this;
    }

    public Result getResult() {
        return result;
    }

    public Response setResult(Result result) {
        this.result = result;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Response setMessage(String message) {
        this.message = message;
        return this;
    }
    public long getTimestamp() {
        return timestamp;
    }
    public Response setTimestamp(long timestamp) {
        this.timestamp = timestamp;
        return this;
    }
}
