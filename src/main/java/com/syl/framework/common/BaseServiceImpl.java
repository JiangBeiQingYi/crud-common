package com.syl.framework.common;

import java.util.List;

/**
 * 基础service 实现类
 *
 * @author syl
 * @create 2018-03-17 22:10
 **/
public class BaseServiceImpl<Bean,DTO> implements BaseService<Bean,DTO>{

    private BaseDao<Bean,DTO> dao;

    public void setDao(BaseDao<Bean,DTO> dao) {
        this.dao = dao;
    }

    @Override
    public String save(Bean bean) {
        return null;
    }

    @Override
    public int delete(String id) {
        return 0;
    }

    @Override
    public int deleteBatch(String[] ids) {
        return 0;
    }

    @Override
    public int update(Bean bean) {
        return 0;
    }

    @Override
    public int updateBatch(Bean bean, String[] ids) {
        return 0;
    }

    @Override
    public List<Bean> queryCondition(QueryCondition condition, DTO bean) {
        return null;
    }
}
