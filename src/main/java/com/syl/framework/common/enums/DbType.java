package com.syl.framework.common.enums;

/**
 * 数据库类型枚举
 *
 * @author syl
 * @create 2018-03-27 21:12
 **/
public enum DbType {
    MYSQL("com.syl.framework.common.limit.impl.MySqlCompute"),
    SQL_SERVER("com.syl.framework.common.limit.impl.SqlServerCompute");

    private String className;

    DbType(String name) {
        this.className= name;
    }

    public String getClassName() {
        return className;
    }

}
