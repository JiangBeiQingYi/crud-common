package com.syl.framework.common;

import com.sun.rowset.internal.Row;
import com.syl.framework.common.enums.DbType;
import com.syl.framework.common.limit.PagingCompute;

/**
 * 简单封装的查询标准类  实现分页,查询条件,排序
 * 待扩展 
 * @author syl
 *
 */
public class QueryCondition {
    private StringBuffer queryColumn = new StringBuffer();
    private StringBuffer orderColumn = new StringBuffer();
    private StringBuffer groupColumn  = new StringBuffer();

    /**
     * 是否 distinct
     */
    private boolean distinct;
    
    /**
     * 是否模糊查询  模糊条件默认为全匹配 '%xxx%'
     */
    private boolean like;
    
    /**
     * 查询条件
     */
    private String query;
    
    /**
     * 排序
     */
    private String orderBy;
    
    /**
     * 是否逆序
     */
    private boolean reverse;
    
    /**
     * 当前页
     */
    private Integer pageCur;
    /**
     * 每页几行
     */
    private Integer pageRow;

    /**
     * 实际分页偏移
     */
    private Integer offset;

    /**
     * 实际行数
     */
    private Integer row;

    public QueryCondition() {
        this(null,null);
    }

    /**
     * 设置默认数据库类型
     * @param pageCur 当前页
     * @param pageRow 每页几行
     */
    public QueryCondition(Integer pageCur, Integer pageRow) {
        this(pageCur,pageRow,DbType.MYSQL);
    }

    public QueryCondition(Integer pageCur, Integer pageRow, DbType dbType) {
        this.pageCur = pageCur;
        this.pageRow = pageRow;
        if(pageCur == null || pageRow == null)return;
        String className = dbType.getClassName();
        try {
            Class<?> clazz = Class.forName(className);
            PagingCompute pc = (PagingCompute) clazz.newInstance();
            pc.compute(this,this.pageCur,this.pageRow);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("DbType classname设置错误");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

    }

    public boolean isDistinct() {
        return distinct;
    }

    public QueryCondition setDistinct(boolean distinct) {
        this.distinct = distinct;
        return this;
    }

    public QueryCondition setQuery(String query) {
        this.queryColumn.append(query).append(" ");
        return this;
    }

    /** 添加查询字段
     * @param column
     * @return
     */
    public QueryCondition addQuery(String column) {
        queryColumn.append(" ");
        queryColumn.append(column);
        queryColumn.append(",");
        return this;
    }
    
    public String getQuery() {
        String q = queryColumn.toString();
        if(q.trim().isEmpty())return " * ";
        this.query = q.substring(0, q.length()-1);//因设置时全部往后累加一个字符
        return query;
    }
    
    public String getOrderBy() {
        String q = orderColumn.toString();
        if(q.trim().isEmpty())return null;
        this.orderBy = q.substring(0, q.length()-1);//因设置时全部往后累加一个字符
        return isReverse() ? orderBy+" desc " : orderBy;
    }

    /** 添加order by字段
     * @param column
     * @return
     */
    public QueryCondition addOrderby(String column) {
        orderColumn.append(" ");
        orderColumn.append(column);
        orderColumn.append(",");
        return this;
    }
    
    public QueryCondition setOrderby(String orderby) {
        orderColumn.append(orderby).append(" ");
        return this;
    }
    
    public boolean isReverse() {
        return reverse;
    }

    /** 是否逆序 
     * @param reverse
     */
    public QueryCondition setReverse(boolean reverse) {
        this.reverse = reverse;
        return this;
    }

    /**
     * 设置当前页
     * @param pageCur
     */
    public void setPageCur(Integer pageCur) {
        this.pageCur = pageCur;
    }

    /**
     * 设置每页显示几行
     * @param pageRow
     */
    public void setPageRow(Integer pageRow) {
        this.pageRow = pageRow;
    }

    /**
     * 是否模糊查询
     * @return
     */
    public boolean isLike() {
        return like;
    }

    public QueryCondition setLike(boolean like) {
        this.like = like;
        return this;
    }

    public QueryCondition setOffset(Integer offset) {
        this.offset = offset;
        return this;
    }

    public QueryCondition setRow(Integer row) {
        this.row = row;
        return this;
    }

    public Integer getOffset() {
        return offset;
    }

    public Integer getRow() {
        return row;
    }

    public static String likeFormat(String str){
        return "%"+str+"%";
    }

    @Override
    public String toString() {
        return "query :"+getQuery() +" like :"+isLike()+" orderBy :"+getOrderBy()+" limit :";
    }
}
