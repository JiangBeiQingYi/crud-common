package com.syl.framework.common;

import java.util.List;

/**
 * 基础service 接口
 *
 * @author syl
 * @create 2018-03-17 22:10
 **/
public interface BaseService<Bean,DTO> {

    /**
     * 保存一条记录
     * @param bean
     * @return
     */
    String save(Bean bean);

    /**
     * 按
     * @param id
     * @return
     */
    int delete(String id);

    int deleteBatch(String[] ids);

    int update(Bean bean);

    int updateBatch(Bean bean,String[] ids);

    List<Bean> queryCondition(QueryCondition condition,DTO bean);
}
